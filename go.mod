module user

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/astaxie/beego v1.11.0 // indirect
	github.com/dimfeld/httptreemux v5.0.1+incompatible
	github.com/jackc/pgx v3.2.0+incompatible
	github.com/jmoiron/sqlx v1.2.0
	github.com/rs/zerolog v1.11.0
	github.com/tkanos/gonfig v0.0.0-20181112185242-896f3d81fadf // indirect
	go.opencensus.io v0.18.0
)
