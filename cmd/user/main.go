package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
	"user/cmd/user/cfg"
	"user/cmd/user/handlers"
	"user/internal/db"

	"github.com/rs/zerolog"
)

func main() {
	log := zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout}).With().Timestamp().Logger()
	cfg, err := cfg.NewConfig()
	if err != nil {
		log.Fatal().Msgf("%s", err)
	}
	db.Init(cfg.DB)
	defer db.DB.Close()

	// Api init
	api := http.Server{
		Addr:           cfg.Host,
		Handler:        handlers.API(&log, cfg, db.DB),
		ReadTimeout:    cfg.ReadWriteTimeout,
		WriteTimeout:   cfg.ReadWriteTimeout,
		MaxHeaderBytes: 1 << 20,
	}

	// Make a channel to listen for errors coming from the listener. Use a
	// buffered channel so the goroutine can exit if we don't collect this error.
	serverErrors := make(chan error, 1)

	// Start the service listening for requests.
	go func() {
		log.Info().Msgf("Start API Listening %s", cfg.Host)
		serverErrors <- api.ListenAndServe()
	}()

	// Make a channel to listen for an interrupt or terminate signal from the OS.
	osSignals := make(chan os.Signal, 1)
	signal.Notify(osSignals, os.Interrupt, syscall.SIGTERM)

	// Blocking main and waiting for shutdown.
	timeout := 30 * time.Second
	select {
	case err := <-serverErrors:
		log.Error().Msgf("Error starting server: %v", err)

	case <-osSignals:
		log.Info().Msg("Start shutdown...")

		// Create context for Shutdown call.
		ctx, cancel := context.WithTimeout(context.Background(), timeout)
		defer cancel()

		// Asking listener to shutdown and load shed.
		if err := api.Shutdown(ctx); err != nil {
			log.Printf("Graceful shutdown did not complete in %v : %v", timeout, err)
			if err := api.Close(); err != nil {
				log.Error().Msgf("Could not stop http server: %v", err)
			}
		}
	}

}
