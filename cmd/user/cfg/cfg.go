package cfg

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"github.com/BurntSushi/toml"
)

const missConfigParameters = "parameters missed in environment or app.conf"

type configStr struct {
	DB               string `toml:"DB_CONNECTION_STRING"`
	Host             string `toml:"HOST"`
	ReadWriteTimeout string `toml:"READ_WRITE_TIMEOUT"`
}

// Config - service config structure
type Config struct {
	DB               string
	Host             string
	ReadWriteTimeout time.Duration
}

// NewConfig - create and validate Config
func NewConfig() (*Config, error) {
	var config *Config
	config, appErr := parseAppConf()
	if appErr == nil {
		return config, nil
	}

	config, err := parseEnvConf()
	if err != nil {
		return nil, fmt.Errorf("%s\r\n%s", appErr, err)
	}

	return config, nil
}

func parseAppConf() (*Config, error) {
	var target Config
	cfgFile, err := ioutil.ReadFile("app.conf")
	if err != nil {
		return nil, err
	}

	source := configStr{}
	err = toml.Unmarshal(cfgFile, &source)
	if err != nil {
		return nil, err
	}

	err = validate(&source)
	if err != nil {
		return nil, err
	}

	target.ReadWriteTimeout, err = time.ParseDuration(source.ReadWriteTimeout)
	if err != nil {
		return nil, err
	}

	target.DB = source.DB
	target.Host = source.Host

	return &target, nil
}

func parseEnvConf() (*Config, error) {
	var target Config
	source := configStr{
		DB:               os.Getenv("DB_CONNECTION_STRING"),
		ReadWriteTimeout: os.Getenv("READ_WRITE_TIMEOUT"),
		Host:             os.Getenv("HOST"),
	}

	err := validate(&source)
	if err != nil {
		return nil, err
	}

	target.ReadWriteTimeout, err = time.ParseDuration(source.ReadWriteTimeout)
	if err != nil {
		return nil, err
	}

	target.Host = source.Host

	return &target, nil
}

func validate(config *configStr) error {
	var errBuilder strings.Builder

	if len(config.DB) == 0 {
		errBuilder.WriteString("DB_CONNECTION_STRING ")
	}

	if len(config.Host) == 0 {
		errBuilder.WriteString("HOST ")
	}

	if len(config.ReadWriteTimeout) == 0 {
		errBuilder.WriteString("READ_WRITE_TIMEOUT ")
	}
	if errBuilder.Len() > 0 {
		errBuilder.WriteString(missConfigParameters)
		return errors.New(errBuilder.String())
	}

	return nil
}
