package handlers

import (
	"user/internal/db"
	"github.com/jmoiron/sqlx"
	"user/internal/output"
	"user/internal/input"

	"context"
	"encoding/json"
	"net/http"
	"time"
	"user/cmd/user/cfg"
	"user/internal/user"
	"user/internal/web"
	"github.com/rs/zerolog"
	"go.opencensus.io/trace"
)

// User - user handler
type User struct {
	config *cfg.Config
	db     *sqlx.DB
}

// Post ...
func (n *User) Post(ctx context.Context, log *zerolog.Logger, w http.ResponseWriter, r *http.Request, params map[string]string) error {
	ctx, span := trace.StartSpan(ctx, "handlers.User.Post")
	defer span.End()

	var v user.UserRequest
	errjson := json.NewDecoder(r.Body).Decode(&v)
	if errjson != nil {
		web.Respond(ctx, log, w, output.OutError{Error: "Ошибка при чтении JSON"}, http.StatusBadRequest)
		return nil
	}

	v.RegistrationDate = time.Now().UTC()

	id, dbErr := user.Post(v, db.DB)
	if dbErr != nil {
		web.Respond(ctx, log, w, dbErr, http.StatusBadRequest)
		return nil
	}

	var u user.User
	u.UUID = *id
	u.Login = v.Login
	u.RegistrationDate = v.RegistrationDate
	web.Respond(ctx, log, w, u, http.StatusOK)

	return nil
}

// GetOne ...
func (n *User) GetOne(ctx context.Context, log *zerolog.Logger, w http.ResponseWriter, r *http.Request, params map[string]string) error {
	ctx, span := trace.StartSpan(ctx, "handlers.User.GetOne")
	defer span.End()

	v, err := user.GetOne(params["id"], n.db)
	if err != nil {
		log.Error().Msgf("%s | %s | %s", r.RemoteAddr, r.URL, err.Error)
		web.Respond(ctx, log, w, &output.OutError{Error: err.OutError}, http.StatusOK)
		return nil
	}

	web.Respond(ctx, log, w, v, http.StatusOK)

	return nil
}

// GetAll ..
func (n *User) GetAll(ctx context.Context, log *zerolog.Logger, w http.ResponseWriter, r *http.Request, params map[string]string) error {
	ctx, span := trace.StartSpan(ctx, "handlers.User.GetAll")
	defer span.End()

	query, errs := input.GetQueryParamFromRequest(r)
	if errs != nil {
		log.Error().Msgf("%s | %s | %s", r.RemoteAddr, r.URL, errs)
		web.Respond(ctx, log, w, &output.OutError{Error: errs.Error()}, http.StatusOK)
		return nil
	}
	v, count, err := user.GetAll(query, n.db)
	if err != nil {
		log.Error().Msgf("%s | %s | %s", r.RemoteAddr, r.URL, err.Error)
		web.Respond(ctx, log, w, &output.OutError{Error: err.OutError}, http.StatusOK)
		return nil
	}

	web.Respond(ctx, log, w, &output.Result{Items: v, TotalCount: count}, http.StatusOK)

	return nil
}

// Put ...
func (n *User) Put(ctx context.Context, log *zerolog.Logger, w http.ResponseWriter, r *http.Request, params map[string]string) error {
	ctx, span := trace.StartSpan(ctx, "handlers.User.Put")
	defer span.End()

	var v user.UserRequest
	//claims := ctx.Input.GetData("claims").(mid.AuthClaims)
	errjson := json.NewDecoder(r.Body).Decode(&v)
	if errjson != nil {
		web.Respond(ctx, log, w, output.OutError{Error: "Ошибка при чтении JSON"}, http.StatusBadRequest)
		return nil
	}

	err := user.Put(params["id"], &v, n.db)
	if err != nil {
		log.Error().Msgf("%s | %s | %s", r.RemoteAddr, r.URL, err.Error)
		web.Respond(ctx, log, w, &output.OutError{Error: err.OutError}, http.StatusOK)
		return nil
	}

	put, err := user.GetOne(params["id"], n.db)
	if err != nil {
		log.Error().Msgf("%s | %s | %s", r.RemoteAddr, r.URL, err.Error)
		web.Respond(ctx, log, w, &output.OutError{Error: err.OutError}, http.StatusOK)
		return nil
	}

	web.Respond(ctx, log, w, put, http.StatusOK)

	return nil
}
