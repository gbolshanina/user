package handlers

import (
	"user/cmd/user/cfg"
	"user/internal/web"

	"net/http"

	"github.com/jmoiron/sqlx"

	"github.com/rs/zerolog"
)

// API - app api handler
func API(log *zerolog.Logger, cfg *cfg.Config, db *sqlx.DB) http.Handler {
	app := web.New(log)

	n := User{
		config: cfg,
		db:     db,
	}

	app.Handle("POST", "/user", n.Post)
	app.Handle("GET", "/user/:id", n.GetOne)
	app.Handle("GET", "/user", n.GetAll)
	app.Handle("PUT", "/user/:id", n.Put)

	return app
}