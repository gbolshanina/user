package input

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

// GetFullSQLQuery Get full query statement with parameters (limit, offset ...)
func GetFullSQLQuery(queryParam *QueryParam, sqlStatSelect *string) {

	// Add query statement
	if queryParam.Query != nil {
		isFirstQueryParam := true
		for key, value := range queryParam.Query {
			if isFirstQueryParam {
				*sqlStatSelect = fmt.Sprintf(`%s WHERE "%s" = '%s'`, *sqlStatSelect, key, value)
				isFirstQueryParam = false
			} else {
				*sqlStatSelect = fmt.Sprintf(`%s WHERE "%s" = '%s'`, *sqlStatSelect, key, value)
			}
		}
	}

	// Add order by statement
	if queryParam.Order != nil {
		isFirstQueryParam := true
		for key, value := range queryParam.Order {
			if isFirstQueryParam {
				*sqlStatSelect = fmt.Sprintf("%s ORDER BY %s %s", *sqlStatSelect, key, value)
				isFirstQueryParam = false
			} else {
				*sqlStatSelect = fmt.Sprintf("%s , %s %s", *sqlStatSelect, key, value)
			}
		}
	}

	// Add offset in statement
	if queryParam.Offset != 0 {
		*sqlStatSelect = fmt.Sprintf("%s OFFSET %d", *sqlStatSelect, queryParam.Offset)
	}

	// Add limit in statement
	if queryParam.Limit != 0 {
		*sqlStatSelect = fmt.Sprintf("%s LIMIT %d", *sqlStatSelect, queryParam.Limit)
	}
}

// GetSQLStatementWithQuery Get query statement with query
func GetSQLStatementWithQuery(queryParam *QueryParam, sqlStatSelect *string) {
	// Add query in statement
	if queryParam.Query != nil {
		isFirstQueryParam := true
		for key, value := range queryParam.Query {
			if isFirstQueryParam {
				*sqlStatSelect = fmt.Sprintf(`%s WHERE "%s" = '%s'`, *sqlStatSelect, key, value)
				isFirstQueryParam = false
			} else {
				*sqlStatSelect = fmt.Sprintf(`%s WHERE "%s" = '%s'`, *sqlStatSelect, key, value)
			}

		}
	}
}

// GetSQLStatementWithOrderLimitOffset Get query statement with parameters limit, offset
func GetSQLStatementWithOrderLimitOffset(queryParam *QueryParam, sqlStatSelect *string) {
	// Add order in statement
	if queryParam.Order != nil {
		isFirstQueryParam := true
		for key, value := range queryParam.Order {
			if isFirstQueryParam {
				*sqlStatSelect = fmt.Sprintf("%s ORDER BY %s '%s'", *sqlStatSelect, key, value)
				isFirstQueryParam = false
			} else {
				*sqlStatSelect = fmt.Sprintf("%s, %s '%s'", *sqlStatSelect, key, value)
			}
		}
	}
	// Add offset in statement
	if queryParam.Offset != 0 {
		*sqlStatSelect = fmt.Sprintf("%s OFFSET %d", *sqlStatSelect, queryParam.Offset)
	}
	// Add limit in statement
	if queryParam.Limit != 0 {
		*sqlStatSelect = fmt.Sprintf("%s LIMIT %d", *sqlStatSelect, queryParam.Limit)
	}
}

// GetQueryParamFromRequest Get QueryParam from request
func GetQueryParamFromRequest(r *http.Request) (queryParam *QueryParam, err error) {

	queryParam = new(QueryParam)
	// query
	if v := r.URL.Query().Get("query"); v != "" {
		for _, cond := range strings.Split(v, ",") {
			kv := strings.SplitN(cond, ":", 2)
			if len(kv) != 2 {
				err := errors.New("Ошибка: неправильные значение в query запросе(key/value)")
				return nil, err
			}
			k, v := kv[0], kv[1]
			queryParam.Query = make(map[string]string)
			queryParam.Query[k] = v
		}
	}

	// order
	if v := r.URL.Query().Get("order"); v != "" {
		for _, cond := range strings.Split(v, ",") {
			kv := strings.SplitN(cond, ":", 2)
			if len(kv) != 2 || (strings.ToLower(kv[1]) != "desc" && strings.ToLower(kv[1]) != "asc") {
				err := errors.New("Ошибка: неправильные значение в query запросе(key/value)")
				return nil, err
			}
			k, v := kv[0], kv[1]
			queryParam.Order = make(map[string]string)
			queryParam.Order[k] = v
		}
	}

	// offset
	if v := r.URL.Query().Get("offset"); v != "" {
		if queryParam.Offset, err = strconv.Atoi(v); err != nil {
			err := errors.New("Ошибка: неправильные значение в запросе(limit)")
			return nil, err
		}
	}

	// limit
	if v := r.URL.Query().Get("limit"); v != "" {
		if queryParam.Limit, err = strconv.Atoi(v); err != nil {
			err := errors.New("Ошибка: неправильные значение в запросе(limit)")
			return nil, err
		}
	}
	return queryParam, nil
}
