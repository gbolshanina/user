package input

// QueryParam ...
type QueryParam struct {
	Query  map[string]string
	Order  map[string]string
	Offset int
	Limit  int
}
