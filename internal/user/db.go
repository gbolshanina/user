package user

import (
	"user/internal/input"

	"github.com/jmoiron/sqlx"
)

// DBPost ...
func DBPost(request UserRequest, db *sqlx.DB) (id *int64, err error) {
	_, err = db.NamedQuery(`INSERT INTO public."user" (login, registration_date) 
	VALUES(:login, :registration_date);`, &request)
	var idc int64
	err = db.Get(&idc, `SELECT MAX(uuid) AS LastID FROM public."user"`)
	if err != nil {
		return nil, err
	}
	id = &idc
	return
}

// DBGetOne ...
func DBGetOne(id string, db *sqlx.DB) (user *User, err error) {
	user = &User{}
	err = db.Get(user, `Select uuid, login, registration_date from public."user" where uuid = $1`, id)
	if err != nil {
		return nil, err
	}
	return user, nil
}

// DBGetAll ...
func DBGetAll(queryParam *input.QueryParam, db *sqlx.DB) (user []User, count int, err error) {
	user = make([]User, 0)
	if queryParam.Query == nil {

		sqlStatSelectCount := `SELECT count(uuid) FROM public."user"`
		if err := db.QueryRow(sqlStatSelectCount).Scan(&count); err != nil {
			return nil, 0, err
		}

		sqlStatSelect := `SELECT uuid, login, registration_date
		FROM public."user"`

		input.GetSQLStatementWithOrderLimitOffset(queryParam, &sqlStatSelect)

		err := db.Select(&user, sqlStatSelect)
		if err != nil {
			return nil, 0, err
		}

	} else {

		sqlStatSelectCount := `SELECT count(uuid) FROM public."user"`
		input.GetSQLStatementWithQuery(queryParam, &sqlStatSelectCount)

		if err := db.QueryRow(sqlStatSelectCount).Scan(&count); err != nil {
			return nil, 0, err
		}

		sqlStatSelect := `SELECT uuid, login, registration_date
				    FROM public."user" `

		input.GetFullSQLQuery(queryParam, &sqlStatSelect)

		err := db.Select(&user, sqlStatSelect)
		if err != nil {
			return nil, 0, err
		}
	}
	return user, count, nil
}

// DBPut ...
func DBPut(id string, v *UserRequest, db *sqlx.DB) (err error) {
	put, err := DBGetOne(id, db)
	if err != nil {
		return err
	}
	if v.Login != "" {
		put.Login = v.Login
	}

	_, err = db.NamedExec(`UPDATE public."user" SET login = :login WHERE uuid= :uuid`, put)
	if err != nil {
		return err
	}
	return nil
}
