package user

import (
	"user/internal/input"
	"user/internal/output"

	"github.com/jmoiron/sqlx"
)

// Post ...
func Post(request UserRequest, db *sqlx.DB) (id *int64, outerr *output.ErrorResult) {
	id, err := DBPost(request, db)
	if err != nil {
		outerr := new(output.ErrorResult)
		outerr.Error = err
		outerr.OutError = "Ошибка при работе с БД: " + err.Error()
		return id, outerr

	}
	return
}

// GetOne ...
func GetOne(id string, db *sqlx.DB) (user *User, outerr *output.ErrorResult) {
	user, err := DBGetOne(id, db)
	if err != nil {
		outerr := new(output.ErrorResult)
		outerr.Error = err
		outerr.OutError = "Ошибка при работе с БД: " + err.Error()
		return nil, outerr
	}
	return user, nil
}

// GetAll ...
func GetAll(queryParam *input.QueryParam, db *sqlx.DB) (user []User, count int, outerr *output.ErrorResult) {
	user, count, err := DBGetAll(queryParam, db)
	if err != nil {
		outerr := new(output.ErrorResult)
		outerr.Error = err
		outerr.OutError = "Ошибка при работе с БД: " + err.Error()
		return nil, 0, outerr
	}
	return user, count, nil
}

// Put ...
func Put(id string, request *UserRequest, db *sqlx.DB) (outerr *output.ErrorResult) {
	err := DBPut(id, request, db)
	if err != nil {
		outerr := new(output.ErrorResult)
		outerr.Error = err
		outerr.OutError = "Ошибка при работе с БД: " + err.Error()
		return outerr

	}
	return
}
