package user

import "time"

//User ...
type User struct {
	UUID             int64     `db:"uuid" json:"uuid"`
	Login            string    `db:"login" json:"login"`
	RegistrationDate time.Time `db:"registration_date" json:"registrationDate"`
}

type UserRequest struct {
	Login            string    `db:"login" json:"login"`
	RegistrationDate time.Time `db:"registration_date" json:"registrationDate"`
}
