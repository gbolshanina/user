package output

type OutError struct {
	Error string `json:"error"`
}
type ErrorResult struct {
	Error    error  `json:"-"`
	OutError string `json:"error"`
}

type Result struct {
	Items      interface{} `json:"items"`
	TotalCount int         `json:"totalCount"`
}
