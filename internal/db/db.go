package db

import (
	"log"
	"time"

	_ "github.com/jackc/pgx/stdlib" // Register pg driver
	"github.com/jmoiron/sqlx"
)

// DB - database connection pool
var DB *sqlx.DB

// Init - initialize db connection pool
func Init(connString string) {
	var err error
	DB, err = sqlx.Connect("pgx", connString)
	if err != nil {
		log.Fatal(err)
	}

	DB.SetConnMaxLifetime(time.Minute * 5)
	DB.SetMaxIdleConns(0)
	DB.SetMaxOpenConns(10)
}
