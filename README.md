## Quick Start

#### Run 

	go run main.go

&emsp;http://localhost:8070/  Отправить запрос можно через файл insomnia

## Задание 3

**Добавить таблицу в postgres**

```sh
CREATE DATABASE user_test OWNER = postgres;
GRANT ALL PRIVILEGES ON database user_test TO postgres;

CREATE TABLE public."user" (
	uuid serial NOT NULL,
	login varchar(255) NULL,
	registration_date timestamp NOT NULL,
	CONSTRAINT user_pk PRIMARY KEY (uuid)
)
WITH (
	OIDS=FALSE
) ;

ALTER TABLE public."user" OWNER TO postgres;
GRANT ALL ON TABLE public."user" TO postgres;
```